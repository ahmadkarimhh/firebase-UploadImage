**Android Kotlin Upload Image Tutorial with Firebase**

Login into google from android studio

```
click icon user at top right page in Android Studio > Login to your account
```

Add Firebase to your project

```
Tools > Firebase > Storage > Login > Connect to Firebase > Add Firebase to project
```





firebase storage rule without login:

```

service firebase.storage {
  match /b/{bucket}/o {
    match /{allPaths=**} {
      //allow read, write: if request.auth != null;
      allow read, write;
    }
  }
}
```

Add this code to AndroidManifest.xml

```
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"></uses-permission>

```

There's 2 file in this project:
1. MainActivity
2. activity_main.xml

Code in MainActivity

```
package com.example.ahmadkarimh.firebaseuploadimage

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.view.View
import android.widget.Toast
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ivImagePerson.setOnClickListener(View.OnClickListener {
            //TODO: select image from phone
            checkPermission()
        })


    }

    val READIMAGE: Int = 253
    fun checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE), READIMAGE)
            }
        }
        loadImage()
    }

    val PICK_IMAGE_CODE = 123
    fun loadImage() {
        //TODO: load image
        var intent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, PICK_IMAGE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_CODE && data != null && resultCode == RESULT_OK) {
            val selectedImage = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = contentResolver.query(selectedImage, filePathColumn, null, null, null)
            cursor.moveToFirst()
            val coulumIndex = cursor.getColumnIndex(filePathColumn[0])
            val picturePath = cursor.getString(coulumIndex)
            cursor.close()
            ivImagePerson.setImageBitmap(BitmapFactory.decodeFile(picturePath))
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            READIMAGE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(applicationContext, "Cannot access your images", Toast.LENGTH_SHORT).show()
                }

            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        }
    }

    fun SaveImageInFirebase() {
        val storage = FirebaseStorage.getInstance()
        val StorageRef = storage.getReferenceFromUrl("gs://fbfirebaseproject-f6a69.appspot.com")

        val imagePath = "gambar1234.jpg"
        val ImageRef = StorageRef.child("images/" + imagePath)
        ivImagePerson.isDrawingCacheEnabled = true
        ivImagePerson.buildDrawingCache()

        val drawable = ivImagePerson.drawable as BitmapDrawable
        val butmap = drawable.bitmap
        val baos = ByteArrayOutputStream()
        butmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val uploadTask = ImageRef.putBytes(data)
        uploadTask.addOnFailureListener {
            Toast.makeText(applicationContext, uploadTask.exception.toString(), Toast.LENGTH_LONG).show()

        }.addOnSuccessListener { taskSnapshot ->
            var DownloadURL = taskSnapshot.downloadUrl!!.toString()

        }
    }


    fun buUpload(view: View) {
        SaveImageInFirebase()
    }




}

```


Code in activity_main.xml

```
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <ImageView
        android:id="@+id/ivImagePerson"
        android:layout_width="131dp"
        android:layout_height="120dp"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="56dp"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:srcCompat="@drawable/common_full_open_on_phone" />

    <Button
        android:id="@+id/buUpload"
        android:layout_width="wrap_content"
        android:layout_height="61dp"
        android:layout_marginTop="80dp"
        android:onClick="buUpload"
        android:text="Upload"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/ivImagePerson" />
</android.support.constraint.ConstraintLayout>
```
